# BattleChoose

Este repositório pertence a disciplina de Projeto de Desenvolvimento do curso de Análise e Desenvolvimento de Sistemas da Faculdade de Tecnologia Senac Pelotas (RS) e conta com a orientação do Me. Angelo Gonçalves da Luz.

## Introdução

O jogo será desenvolvido utilizando a linguagem de programação C# e contará com recursos da ferramenta [Unity](https://unity.com/pt). Será um jogo 2D de batalha em turnos do gênero casual, para plataforma mobile.

## Requisitos

### Git

É necessário ter o Git instalado se quiser fazer o download via SSH ou HTTP. Site oficial do [Git](https://git-scm.com/).

### Unity

O Unity necessita de alguns requisitos do sistema para funcionar corretamente, eles podem ser encontrados no site oficial clicando [aqui](https://unity3d.com/pt/unity/system-requirements).

## Clone ou Download

Clone via SSH:

`git@gitlab.com:senac-tcc-2019/paulohenrique.git`

Clone via HTTP:

`https://gitlab.com/senac-tcc-2019/paulohenrique.git`

Além disto, é possível baixar o projeto em outros formatos clicando na opção "Download".

## Documentação

Está disponivel na [WIKI](https://gitlab.com/senac-tcc-2019/paulohenrique/wikis/home) deste projeto.

## Licença

Este projeto conta com a seguinte licença de uso: [MIT](https://gitlab.com/senac-tcc-2019/paulohenrique/blob/master/LICENSE).